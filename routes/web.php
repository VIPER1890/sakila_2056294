<?php


use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Ruta de controladorDB:
Route::get("categorias" , "CategoriaController@index");
//Ruta de mostrar el formulario para crear categoria
Route::get("categorias/create" , "CategoriaController@create");
//Ruta para guardar la nueva categoria en BD
Route::post("categorias/store" , "CategoriaController@store");
Route::get("categorias/edit/{category_id}" , "CategoriaController@edit");
Route::post("categorias/update/{category_id}" , "CategoriaController@update");

